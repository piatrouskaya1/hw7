﻿using System;

abstract class Animal
{
    public abstract void MakeSound();
}
class Dog : Animal
{
    public override void MakeSound()
    {
        Console.WriteLine("Dog's sound");
    }
}
class Cat : Animal
{
    public override void MakeSound()
    {
        Console.WriteLine("Cat's sound");
    }
}
class PetShop
{
    private Animal[] animals;
    public PetShop(Animal[] animals)
    {
        this.animals = animals;
    }
    public void MakeAllSounds()
    {
        Console.WriteLine("Animals' sounds in the shop:");
        foreach (var animal in animals)
        {
            animal.MakeSound();
        }
    }
}
class Program
{
    static void Main()
    {
        Animal[] animals = new Animal[]
        {
            new Dog(),
            new Cat(),
            new Dog(),
            new Cat()
        };
        PetShop petShop = new PetShop(animals);
        petShop.MakeAllSounds();
    }
}