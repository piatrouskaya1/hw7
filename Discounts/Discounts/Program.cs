﻿using System;
public abstract class DiscountStrategy
{
    public abstract double ApplyDiscount(double originalPrice);
}
class FixedDiscount : DiscountStrategy
{
    private double fixedAmount;
    public FixedDiscount(double fixedAmount)
    {
        this.fixedAmount = fixedAmount;
    }
    public override double ApplyDiscount(double originalPrice)
    {
        return originalPrice - fixedAmount;
    }
}
class PercentageDiscount : DiscountStrategy
{
    private double percentage;

    public PercentageDiscount(double percentage)
    {
        this.percentage = percentage;
    }
    public override double ApplyDiscount(double originalPrice)
    {
        return originalPrice * (1 - percentage / 100);
    }
}
class Product
{
    public string Name { get; set; }
    public double Price { get; set; }
    public Product(string name, double price)
    {
        Name = name;
        Price = price;
    }
}
class ShoppingCart
{
    public DiscountStrategy discountStrategy;
    public Product[] products;
    public ShoppingCart(Product[] products)
    {
        this.products = products;
        discountStrategy = null;
    }
    public void SetDiscountStrategy(DiscountStrategy strategy)
    {
        discountStrategy = strategy;
    }
    public double CalculateTotal()
    {
        double total = 0;
        foreach (var product in products)
        {
            total += discountStrategy.ApplyDiscount(product.Price);
        }
        return total;
    }
}
class Program
{
    static void Main()
    {
        Product[] products = new Product[]
        {
            new Product("Item 1", 100.0),
            new Product("Item 2", 50.0),
            new Product("Item 3", 200.0)
        };
        ShoppingCart cart = new ShoppingCart(products);
        DiscountStrategy percentageDiscount = new PercentageDiscount(10); 
        DiscountStrategy fixedDiscount = new FixedDiscount(30); 

        cart.SetDiscountStrategy(percentageDiscount);

        double totalWithDiscount = cart.CalculateTotal();
        Console.WriteLine($"General amount with discount: {totalWithDiscount}");

        cart.SetDiscountStrategy(fixedDiscount);

        double totalWithFixedDiscount = cart.CalculateTotal();
        Console.WriteLine($"General amount with fixed discount: {totalWithFixedDiscount}");
    }
}